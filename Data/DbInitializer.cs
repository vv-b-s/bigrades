using System;
using System.Collections.Generic;
using System.Linq;
using BIGrades.Models;
using Microsoft.CodeAnalysis.Differencing;

namespace BIGrades.Data
{
    public class DbInitializer
    {
        public static void Initialize(BIGradesDbContext context)
        {
            AddStreams(context);
            context.SaveChanges();
            
            AddStudents(context);
            context.SaveChanges();
            
            AddGroups(context);
            context.SaveChanges();

            AddCourses(context);
            context.SaveChanges();
            
            AddEnrollments(context);
            context.SaveChanges();
            
            AddYears(context);
            context.SaveChanges();
            
            AddCourses(context);
            context.SaveChanges();
            
            AddEduPrograms(context);
            context.SaveChanges();
            
            AddAssignmentTypes(context);
            context.SaveChanges();
            
            AddAssignments(context);
            context.SaveChanges();
            
            AddGrades(context);
            context.SaveChanges();
            
        }

        private static void AddGroups(BIGradesDbContext context)
        {
            var streams = context.Streams.ToList();
            
            if (!context.Groups.Any())
            {
                var groups = new Group[]
                {

                    new Group
                    {
                        Id = Guid.NewGuid(), GroupNumber = "1647",
                        Stream = streams[0]
                    },
                    new Group
                    {
                        Id = Guid.NewGuid(), GroupNumber = "1947",
                        Stream = streams[0]
                    }
                };

                foreach (var group in groups)
                {
                    context.Groups.Add(group);
                }
            }
        }
        
        private static void AddStreams(BIGradesDbContext context)
        {
            if (!context.Streams.Any())
            {
                var streams = new List<Stream>()
                {
                    new Stream {Id = Guid.NewGuid(), StreamNumber = "199"}

                };

                foreach (var stream in streams)
                {
                    context.Streams.Add(stream);
                }
            }
        }
        
        private static void AddStudents(BIGradesDbContext context)
        {
            if (!context.Students.Any())
            {
                var students = new Student[]
                {
                    new Student{ FirstName="Lilly", MidName="Vaseva", LastName="Vaseva",
                        FacultyNum="18118016", PIN="9302027475", Id= Guid.NewGuid()},
                    new Student{ FirstName="Ivan", MidName="Petrov", LastName="Georgiev",
                        FacultyNum="18118017", PIN="9305096459", Id= Guid.NewGuid()},
                    new Student{ FirstName="Petya", MidName="Petrova", LastName="Ivanova",
                        FacultyNum="18118018", PIN="9306089052", Id= Guid.NewGuid()},
                    new Student{ FirstName="Vanya", MidName="Petrova", LastName="Georgieva",
                        FacultyNum="18118019", PIN="9305096459", Id= Guid.NewGuid()},
                    new Student{ FirstName="Vasil", MidName="Petrov", LastName="Georgiev",
                        FacultyNum="18118020", PIN="9305096459", Id= Guid.NewGuid()},
                    new Student{ FirstName="Mitko", MidName="Petrov", LastName="Georgiev",
                        FacultyNum="18118021", PIN="9305096459", Id= Guid.NewGuid()},
                    new Student{ FirstName="Galya", MidName="Petrova", LastName="Georgieva",
                        FacultyNum="18118022", PIN="9305096459", Id= Guid.NewGuid()}
                };
                
                foreach (var s in students)
                {
                    context.Students.Add(s);
                }
            }

        }

        private static void AddEnrollments(BIGradesDbContext context)
        {
            var groups = context.Groups.ToList();
            var students = context.Students.ToList();
            
            if (!context.Enrollments.Any())
            {
                var enrollments = new StudentEnrollment[]
                {
                    new StudentEnrollment{ Student=students[0], Active=true,
                        EnrollmentDate=DateTime.Today, Group=groups[0], Id= Guid.NewGuid() },
                    new StudentEnrollment{ Student=students[1], Active=true,
                        EnrollmentDate=DateTime.Today, Group=groups[0], Id= Guid.NewGuid() },
                    new StudentEnrollment{ Student=students[2], Active=true,
                        EnrollmentDate=DateTime.Today, Group=groups[0], Id= Guid.NewGuid() },
                    new StudentEnrollment{ Student=students[3], Active=true,
                        EnrollmentDate=DateTime.Today, Group=groups[0], Id= Guid.NewGuid() },
                    new StudentEnrollment{ Student=students[4], Active=true,
                        EnrollmentDate=DateTime.Today, Group=groups[1], Id= Guid.NewGuid() },
                    new StudentEnrollment{ Student=students[5], Active=true,
                        EnrollmentDate=DateTime.Today, Group=groups[1], Id= Guid.NewGuid() },
                    new StudentEnrollment{ Student=students[6], Active=true,
                        EnrollmentDate=DateTime.Today, Group=groups[1], Id= Guid.NewGuid() },
                    new StudentEnrollment{ Student=students[0], Active=false,
                        EnrollmentDate=new DateTime(2016,9,15), Group=groups[0], Id= 
                            Guid.NewGuid() }
                };

                foreach (var enrollment in enrollments)
                {
                    context.Enrollments.Add(enrollment);
                }
            }
        }

        private static void AddYears(BIGradesDbContext context)
        {
            if (!context.Grades.Any())
            {
                context.EduYears.Add(new EduYear {Id = Guid.NewGuid(), Name = "2018/2019"});
            }
        }

        private static void AddCourses(BIGradesDbContext context)
        {
            if (!context.Courses.Any())
            {
                var courses = new Course[]
                {
                    new Course
                    {
                        Active = true, AvailableFrom = new DateTime(2018 / 9 / 15),
                        Name = "Web Application Development",
                        Description = "EF Core, Asp.net Core", Id = Guid.NewGuid()
                    },
                    new Course
                    {
                        Active = true, AvailableFrom = new DateTime(2011 / 9 / 15),
                        Name = "Application Development",
                        Description = "C#, Windows Forms", Id = Guid.NewGuid()
                    }
                };

                foreach (var course in courses)
                {
                    context.Courses.Add(course);
                }
            }
        }

        private static void AddEduPrograms(BIGradesDbContext context)
        {
            var years = context.EduYears.ToList();
            var courses = context.Courses.ToList();
            
            if (!context.EduPrograms.Any())
            {
                var eduPrograms = new EduProgram[]
                {
                    new EduProgram
                    {
                        Id = Guid.NewGuid(), Active = true, Course =
                            courses[0],
                        EduYear = years[0], Semester = "winter"
                    },
                    new EduProgram
                    {
                        Id = Guid.NewGuid(), Active = true, Course =
                            courses[1],
                        EduYear = years[0], Semester = "summer"
                    }
                };

                foreach (var eduProgram in eduPrograms)
                {
                    context.EduPrograms.Add(eduProgram);
                }
            }
        }

        private static void AddProgramEnrollments(BIGradesDbContext context)
        {
            var groups = context.Groups.ToList();
            var eduPrograms = context.EduPrograms.ToList();
            
            if (!context.EduProgramEnrollments.Any())
            {
                 var eduProgramEnrollments = new EduProgramEnrollment[]
                {
                    new EduProgramEnrollment{ EduProgram= eduPrograms[0], Group=groups[0],
                        Id = Guid.NewGuid() },
                    new EduProgramEnrollment{ EduProgram= eduPrograms[0], Group=groups[1],
                        Id = Guid.NewGuid() },
                    new EduProgramEnrollment{ EduProgram= eduPrograms[1], Group=groups[0],
                        Id = Guid.NewGuid() },
                    new EduProgramEnrollment{ EduProgram= eduPrograms[1], Group=groups[1],
                        Id = Guid.NewGuid() },
                    new EduProgramEnrollment{ EduProgram= eduPrograms[0], Group=groups[0],
                    Id = Guid.NewGuid() }
                };

                foreach (var e in eduPrograms)
                {
                    context.EduPrograms.Add(e);
                }
            }
        }

        private static void AddAssignmentTypes(BIGradesDbContext context)
        {
            if (!context.AssignmentTypes.Any())
            {
                var assignmentTypes = new AssignmentType[]
                {
                    new AssignmentType{ Id= Guid.NewGuid(), Name="HW"},
                    new AssignmentType{ Id= Guid.NewGuid(), Name="CW"},
                    new AssignmentType{ Id= Guid.NewGuid(), Name="Bonus"},
                    new AssignmentType{ Id= Guid.NewGuid(), Name="Course Project"},
                    new AssignmentType{ Id= Guid.NewGuid(), Name="Test"}
                };

                foreach (var assignmentType in assignmentTypes)
                {
                    context.AssignmentTypes.Add(assignmentType);
                }
            }
        }

        private static void AddAssignments(BIGradesDbContext context)
        {
            var eduPrograms = context.EduPrograms.ToList();
            var assignmentTypes = context.AssignmentTypes.ToList();
            
            if (!context.Assignments.Any())
            {
                var assignments = new Assignment[]
                {
                    new Assignment{ Id= Guid.NewGuid(), Name = "HW 1",
                        AssignmentType=assignmentTypes[0],
                        Weight =0.1m, EduProgram= eduPrograms[0]},
                    new Assignment{ Id= Guid.NewGuid(), Name = "HW 2",
                        AssignmentType=assignmentTypes[0],
                        Weight =0.1m, EduProgram= eduPrograms[0]},
                    new Assignment{ Id= Guid.NewGuid(), Name = "CW 1",
                        AssignmentType=assignmentTypes[1],
                        Weight =0.1m, EduProgram= eduPrograms[0]},
                    new Assignment{ Id= Guid.NewGuid(), Name = "CW 2",
                        AssignmentType=assignmentTypes[1],
                        Weight =0.1m, EduProgram= eduPrograms[0]},
                    new Assignment{ Id= Guid.NewGuid(), Name = "Course Project",
                        AssignmentType=assignmentTypes[3],
                        Weight =0.4m, EduProgram= eduPrograms[0]},
                    new Assignment{ Id= Guid.NewGuid(), Name = "CW 1",
                        AssignmentType=assignmentTypes[1],
                        Weight =0.1m, EduProgram= eduPrograms[1]},
                    new Assignment{ Id= Guid.NewGuid(), Name = "CW 2",
                        AssignmentType=assignmentTypes[1],
                        Weight =0.1m, EduProgram= eduPrograms[1]},
                    new Assignment{ Id= Guid.NewGuid(), Name = "Course Project",
                        AssignmentType=assignmentTypes[3],
                        Weight =0.4m, EduProgram= eduPrograms[1]},
                };

                foreach (var assignment in assignments)
                {
                    context.Assignments.Add(assignment);
                }
            }
        }

        private static void AddGrades(BIGradesDbContext context)
        {
            var assignments = context.Assignments.ToList();
            var students = context.Students.ToList();
            
            if (!context.Grades.Any())
            {
                var grades = new Grade[]
                {
                    new Grade{ Assignment= assignments[0], Student=students[0], Id=
                            Guid.NewGuid(), Value=1,
                        Delivered =new DateTime(2018,10,12) },
                    new Grade{ Assignment= assignments[1], Student=students[0], Id=
                            Guid.NewGuid(), Value=1,
                        Delivered =new DateTime(2018,10,12) },
                    new Grade{ Assignment= assignments[2], Student=students[0], Id=
                            Guid.NewGuid(), Value=1,
                        Delivered =new DateTime(2018,10,12) },
                    
                    new Grade{ Assignment= assignments[3], Student=students[0], Id=
                        Guid.NewGuid(), Value=1,
                    Delivered =new DateTime(2018,10,12) },
                };

                foreach (var grade in grades)
                {
                    context.Grades.Add(grade);
                }
            }
        }
    }
}