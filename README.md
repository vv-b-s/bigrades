### Deployment
Deployment of this project follows the steps from [ASP.NET MVC Config](https://gitlab.com/vv-b-s/asp.net-mvc-config/blob/master/readme.md).

### Data migration

appsettings.json:
```json
{
  "ConnectionStrings": {
    "Context": "Server=localhost,1433;Database=TestEF;User=sa;Password=Adm!n!strAt0r;"
  },
  
  "Logging": {
    "LogLevel": {
      "Default": "Warning"
    }
  },
  "AllowedHosts": "*"
}

```

Required packages: 
```bash
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.EntityFrameworkCore.Tools
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
```


Migration commands:
```bash
dotnet ef migrations add InitialCreate
dotnet ef database update
```

### Scaffolding
In Razor Pages project scaffolding is done by the following command:
```
dotnet aspnet-codegenerator razorpage -m ModelName -dc BIGrades.Models.BIGradesDbContext -udl -outDir PagesModelName

```