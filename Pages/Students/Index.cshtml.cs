using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BIGrades.Models;

namespace BIGrades.Pages.Students
{
    public class IndexModel : PageModel
    {
        private readonly BIGrades.Models.BIGradesDbContext _context;

        public IndexModel(BIGrades.Models.BIGradesDbContext context)
        {
            _context = context;
        }

        public IList<Student> Student { get;set; }

        public async Task OnGetAsync()
        {
            Student = await _context.Students.ToListAsync();
        }
    }
}
