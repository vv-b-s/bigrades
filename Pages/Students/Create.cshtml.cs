using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using BIGrades.Models;

namespace BIGrades.Pages.Students
{
    public class CreateModel : PageModel
    {
        private readonly BIGrades.Models.BIGradesDbContext _context;

        public CreateModel(BIGrades.Models.BIGradesDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Student Student { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            
            
            var emptyStudent = new Student();

            var emptyStudentUpdated = await TryUpdateModelAsync(emptyStudent, "student",
                s => s.FirstName,
                s => s.MidName,
                s => s.LastName,
                s => s.PIN,
                s => s.FacultyNum,
                s => s.EnrollmentDate);

            if (!emptyStudentUpdated) return null;
            _context.Students.Add(emptyStudent);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");

        }
    }
}