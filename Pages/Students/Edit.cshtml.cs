using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BIGrades.Models;

namespace BIGrades.Pages.Students
{
    public class EditModel : PageModel
    {
        private readonly BIGrades.Models.BIGradesDbContext _context;

        public EditModel(BIGrades.Models.BIGradesDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Student Student { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Student = await _context.Students.FindAsync(id);

            if (Student == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid? id)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var studentToUpdate = await _context.Students.FindAsync(id);
            
            var studentUpdated = await TryUpdateModelAsync(studentToUpdate, "student",
                s => s.FirstName,
                s => s.MidName,
                s => s.LastName,
                s => s.PIN,
                s => s.FacultyNum,
                s => s.EnrollmentDate);

            if (!studentUpdated) return Page();
            await _context.SaveChangesAsync();
            return RedirectToPage("./Index");
            
        }

        private bool StudentExists(Guid id)
        {
            return _context.Students.Any(e => e.Id == id);
        }
    }
}
