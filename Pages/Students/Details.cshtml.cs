using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BIGrades.Models;

namespace BIGrades.Pages.Students
{
    public class DetailsModel : PageModel
    {
        private readonly BIGrades.Models.BIGradesDbContext _context;

        public DetailsModel(BIGrades.Models.BIGradesDbContext context)
        {
            _context = context;
        }

        public Student Student { get; set; }
        public Dictionary<string, List<Grade>> CourseGrade { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Student = await _context.Students
                .Include(s => s.Enrollments)
                .ThenInclude(e => e.Group)
                .Include(s => s.Grades)
                .ThenInclude(g => g.Assignment)
                .ThenInclude(a => a.EduProgram)
                .ThenInclude(ep => ep.Course)
                .FirstOrDefaultAsync(m => m.Id == id);
            
            CourseGrade = new Dictionary<string, List<Grade>>();
            foreach (var grade in Student.Grades)
            {
                var courseName = grade.Assignment.EduProgram.Course.Name;
                if (!CourseGrade.ContainsKey(courseName))
                {
                    CourseGrade[courseName] = new List<Grade>();
                }
                
                CourseGrade[courseName].Add(grade);
            }


            if (Student == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
