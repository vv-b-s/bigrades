using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BIGrades.Models;

namespace BIGrades.Streams
{
    public class DeleteModel : PageModel
    {
        private readonly BIGrades.Models.BIGradesDbContext _context;

        public DeleteModel(BIGrades.Models.BIGradesDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Stream Stream { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Stream = await _context.Streams.FirstOrDefaultAsync(m => m.Id == id);

            if (Stream == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Stream = await _context.Streams.FindAsync(id);

            if (Stream != null)
            {
                _context.Streams.Remove(Stream);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
