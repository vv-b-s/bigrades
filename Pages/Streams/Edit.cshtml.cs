using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BIGrades.Models;

namespace BIGrades.Streams
{
    public class EditModel : PageModel
    {
        private readonly BIGrades.Models.BIGradesDbContext _context;

        public EditModel(BIGrades.Models.BIGradesDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Stream Stream { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Stream = await _context.Streams.FirstOrDefaultAsync(m => m.Id == id);

            if (Stream == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Stream).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StreamExists(Stream.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool StreamExists(Guid id)
        {
            return _context.Streams.Any(e => e.Id == id);
        }
    }
}
