using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BIGrades.Models;

namespace BIGrades.Streams
{
    public class DetailsModel : PageModel
    {
        private readonly BIGrades.Models.BIGradesDbContext _context;

        public DetailsModel(BIGrades.Models.BIGradesDbContext context)
        {
            _context = context;
        }

        public Stream Stream { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

//            Stream = await _context.Streams.FirstOrDefaultAsync(m => m.Id == id);

            Stream = await _context.Streams
                .Include(s => s.Groups)
                .ThenInclude(g => g.StudentEnrollments)
                .AsNoTracking()
                .FirstAsync(m => m.Id == id);
            
            if (Stream == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
