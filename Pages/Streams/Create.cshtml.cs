using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using BIGrades.Models;

namespace BIGrades.Streams
{
    public class CreateModel : PageModel
    {
        private readonly BIGrades.Models.BIGradesDbContext _context;

        public CreateModel(BIGrades.Models.BIGradesDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Stream Stream { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Streams.Add(Stream);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}