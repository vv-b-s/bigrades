using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BIGrades.Models;

namespace BIGrades.Streams
{
    public class IndexModel : PageModel
    {
        private readonly BIGrades.Models.BIGradesDbContext _context;

        public IndexModel(BIGrades.Models.BIGradesDbContext context)
        {
            _context = context;
        }

        public IList<Stream> Stream { get;set; }

        public async Task OnGetAsync()
        {
            Stream = await _context.Streams.ToListAsync();
        }
    }
}
