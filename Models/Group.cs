using System.Collections.Generic;

namespace BIGrades.Models
{
    public class Group : Entity
    {
        public string GroupNumber { get; set; }
        public Stream Stream { get; set; }

        public ICollection<StudentEnrollment> StudentEnrollments { get; set; }
        public ICollection<EduProgramEnrollment> Courses { get; set; }
    }
}