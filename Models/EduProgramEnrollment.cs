using System;

namespace BIGrades.Models
{
    public class EduProgramEnrollment : Entity
    {
        public Group Group { get; set; }
        public EduProgram EduProgram { get; set; }
    }
}