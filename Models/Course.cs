using System;
using System.Collections.Generic;

namespace BIGrades.Models
{
    public class Course : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime AvailableFrom { get; set; }
        public bool Active { get; set; }

        public ICollection<EduProgram> EduPrograms { get; set; }
    }
}