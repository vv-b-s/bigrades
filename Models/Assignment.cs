using System.Collections.Generic;

namespace BIGrades.Models
{
    public class Assignment : Entity
    {
        public EduProgram EduProgram { get; set; }
        public AssignmentType AssignmentType { get; set; }
        public string Name { get; set; }
        public decimal Weight { get; set; }
    }
}