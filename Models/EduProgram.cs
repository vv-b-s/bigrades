using System.Collections.Generic;

namespace BIGrades.Models
{
    public class EduProgram : Entity
    {
        public EduYear EduYear { get; set; }
        public Course Course { get; set; }
        public string Semester { get; set; }
        public bool Active { get; set; }

        public ICollection<Assignment> Assignments { get; set; }
    }
}