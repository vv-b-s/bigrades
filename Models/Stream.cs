using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BIGrades.Models
{
    public class Stream : Entity
    {
        [Required(ErrorMessage = "No stream without number .... :")]
        public string StreamNumber { get; set; }

        public ICollection<Group> Groups { get; set; }
    }
}