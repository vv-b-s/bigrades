using Microsoft.EntityFrameworkCore;

namespace BIGrades.Models
{
    public class BIGradesDbContext : DbContext
    {
        public BIGradesDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<AssignmentType> AssignmentTypes { get; set; }
        public DbSet<EduProgram> EduPrograms { get; set; }
        public DbSet<EduProgramEnrollment> EduProgramEnrollments { get; set; }
        public DbSet<EduYear> EduYears { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Stream> Streams { get; set; }
        public DbSet<StudentEnrollment> Enrollments { get; set; }
    }
}