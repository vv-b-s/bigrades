using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BIGrades.Models
{
    public class Student : Entity
    {
        [Required]
        [RegularExpression(@"^\d{10}$")]
        public string FacultyNum { get; set; }
        public string FirstName { get; set; }
        public string MidName { get; set; }
        public string LastName { get; set; }
        public string PIN { get; set; }
        public DateTime EnrollmentDate { get; set; }

        public ICollection<StudentEnrollment> Enrollments { get; set; }
        public ICollection<Grade> Grades { get; set; }
    }
}