using System.Collections;
using System.Collections.Generic;

namespace BIGrades.Models
{
    public class EduYear : Entity
    {
        public string Name { get; set; }

        public ICollection<EduProgram> EduPrograms { get; set; }
    }
}