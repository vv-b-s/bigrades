using System;

namespace BIGrades.Models
{
    public class Grade : Entity
    {
        public Student  Student { get; set; }
        public Assignment Assignment { get; set; }
        public DateTime Delivered { get; set; }

        public decimal Value { get; set; }
    }
}