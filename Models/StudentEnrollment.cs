using System;

namespace BIGrades.Models
{
    public class StudentEnrollment : Entity
    {
        public Student Student { get; set; }
        public Group Group { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public bool Active { get; set; }
    }
}